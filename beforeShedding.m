% Looking at the spatial distribution of microbes right before shedding

clear all; close all; clc;

p.r = 1.5;
p.tao = 0.5;
p.L = 1;
p.k = 1;

M = @(x) exp(p.r*p.tao)*exp(-x.^2/(4*p.k*p.tao))./sqrt(pi*p.k*p.tao);

x = linspace(0,10,100);
plot(x,M(x))

volumeRemaining = exp(p.r*p.tao)*(1-erf(p.L/sqrt(4*p.k*p.tao)));


function locationsNew = reflectedBrownianMotion(locationsOld, dt, mu, sigma)
% locationsOld needs to be a row vector
flag = size(locationsOld);
if flag(2) == 1
    locationsOld = locationsOld';
end
rescaledmu = mu/sigma;
Y=sqrt(dt)*randn(size(locationsOld)); % see step 2 of Algotirhm 5.26
U=rand(size(locationsOld)); % see step 3 of Algotirhm 5.26
M=(Y + sqrt(Y.^2-2*dt*log(U)))/2; % see step 3 of Algotirhm 5.26
rescaledLocations=max([M-Y;locationsOld/sigma+dt*rescaledmu-Y]); % see step 4 of Algotirhm 5.26
locationsNew = sigma*rescaledLocations;
end
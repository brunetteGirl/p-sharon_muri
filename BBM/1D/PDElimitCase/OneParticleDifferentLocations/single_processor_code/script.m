clear all; close all; clc;

addpath /home/zhou.494/Documents/MATLAB/p-Sharon_MURI/BBM/1D/sheddingRate/CompareWithPDE

tic
NSimulations = 5000;
startingLocationsVec = 1:10;
smallestSheddingLength = 0.0025;
sheddingRate = 1;
% sheddingRateVec = [1 2 3 4 5 6 8 10 12 15 20 30 40 60 120]; %shedding once for every 1 BBM steps, 2 steps, ... 20 steps.
% LVec = smallestSheddingLength.*sheddingRateVec; % shedding length in each shedding event
% averageExtinctionTimeVec = zeros(size(LVec));
probExtinctionVec = zeros(size(startingLocationsVec));

for ind = 1:length(startingLocationsVec)
    startingLocation = startingLocationsVec(ind);
    [averageExtinctionTime,probExtinction] = simulationsFunc(NSimulations,startingLocation,smallestSheddingLength,sheddingRate);
    % averageExtinctionTimeVec(ind) = averageExtinctionTime;
    probExtinctionVec(ind) = probExtinction;
end
toc

% fig1 = figure(1)
% plot(LVec,averageExtinctionTimeVec,'*')
% xlabel('Shedding length in each shedding event','FontSize',30);
% ylabel('Average extinction time','FontSize',30);
% % set(gca,'XLim',[0,20]);
% set(gca,'YLim',[0.5,1]);
% set(gca,'Box','Off')
% set(gca,'FontSize',30);
% set(gca,'TickDir','out');
% print(fig1, '-depsc','averageExtinctionTime.eps')
% save averageExtinctionTimeVec.dat averageExtinctionTimeVec -ascii

fig1 = figure(1)
plot(startingLocationsVec,probExtinctionVec,'*')
xlabel('Starting location of the initial particle','FontSize',30);
ylabel('Probability of Extinction','FontSize',30);
set(gca,'XLim',[0,startingLocationsVec(end)]);
set(gca,'YLim',[0.5,1]);
set(gca,'Box','Off')
set(gca,'FontSize',30);
set(gca,'TickDir','out');
print(fig1, '-depsc','probExtinctionVecWRTstartingLocations.eps')
save startingLocationsVec.dat startingLocationsVec -ascii
save probExtinctionVecWRTstartingLocations.dat probExtinctionVec -ascii

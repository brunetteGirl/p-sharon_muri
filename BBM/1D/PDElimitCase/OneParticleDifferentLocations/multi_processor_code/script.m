clear all; close all; clc;

tic
NSimulations = 5000;
% smallestSheddingLengthVec = linspace(0.0075,0.0975,19);
smallestSheddingLengthVec = linspace(0.0001,0.001,10);
startingLocationsVec = 1:10;
parfor ind = 1:length(smallestSheddingLengthVec)
    smallestSheddingLength = smallestSheddingLengthVec(ind);
    computeQOfxt(NSimulations,smallestSheddingLength,startingLocationsVec);
end
toc

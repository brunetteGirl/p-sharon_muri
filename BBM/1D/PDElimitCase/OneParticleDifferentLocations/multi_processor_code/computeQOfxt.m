function computeQOfxt(NSimulations,smallestSheddingLength,startingLocationsVec)
addpath /home/zhou.494/Documents/MATLAB/p-Sharon_MURI/BBM/1D/sheddingRate/CompareWithPDE/OneParticleDifferentLocations

sheddingRate = 1;
probExtinctionVec = zeros(size(startingLocationsVec));
averageExtinctionTimeVec = zeros(size(startingLocationsVec));

for ind = 1:length(startingLocationsVec)
    startingLocation = startingLocationsVec(ind);
    [averageExtinctionTime,probExtinction] = simulationsFunc(NSimulations,startingLocation,smallestSheddingLength,sheddingRate);
    averageExtinctionTimeVec(ind) = averageExtinctionTime;
    probExtinctionVec(ind) = probExtinction;
end

% fig1 = figure(1)
% plot(LVec,averageExtinctionTimeVec,'*')
% xlabel('Shedding length in each shedding event','FontSize',30);
% ylabel('Average extinction time','FontSize',30);
% % set(gca,'XLim',[0,20]);
% set(gca,'YLim',[0.5,1]);
% set(gca,'Box','Off')
% set(gca,'FontSize',30);
% set(gca,'TickDir','out');
% print(fig1, '-depsc','averageExtinctionTime.eps')
save(['averageExtinctionTimeVecL' num2str(smallestSheddingLength) '.dat'],'averageExtinctionTimeVec','-ascii')

fig1 = figure(1)
plot(startingLocationsVec,probExtinctionVec,'*')
xlabel('Shedding length in each shedding event','FontSize',30);
ylabel('Probability of Extinction','FontSize',30);
set(gca,'XLim',[0,startingLocationsVec(end)]);
set(gca,'YLim',[0.5,1]);
set(gca,'Box','Off')
set(gca,'FontSize',30);
set(gca,'TickDir','out');
title(['Shedding length =' num2str(smallestSheddingLength)]);
%print(fig1, '-depsc','probExtinctionVecWRTstartingLocations.eps')
print(fig1,'-depsc',['FIGprobExtinctionVecWRTstartingLocations' num2str(smallestSheddingLength) '.eps'])
save(['startingLocationsVecL' num2str(smallestSheddingLength) '.dat'],'startingLocationsVec','-ascii')
save(['probExtinctionVecWRTstartingLocationsL' num2str(smallestSheddingLength) '.dat'],'probExtinctionVec','-ascii')
end
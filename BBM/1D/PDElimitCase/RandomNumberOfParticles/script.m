clear all; close all; clc;

tic
NSimulations = 100;
smallestSheddingLength = 0.0025;
sheddingRateVec = 1;
% sheddingRateVec = [1 2 3 4 5 6 8 10 12 15 20 30 40 60 120]; %shedding once for every 1 BBM steps, 2 steps, ... 20 steps.
LVec = smallestSheddingLength.*sheddingRateVec; % shedding length in each shedding event
averageExtinctionTimeVec = zeros(size(LVec));
probExtinctionVec = zeros(size(LVec));
startingLocation = 5; % domain is 0 to 10

for ind = 1:length(LVec)
    L = LVec(ind);
    sheddingRate = sheddingRateVec(ind);
    [averageExtinctionTime,probExtinction] = simulationsFunc(NSimulations,startingLocation,L,sheddingRate);
    averageExtinctionTimeVec(ind) = averageExtinctionTime;
    probExtinctionVec(ind) = probExtinction;
end
toc

fig1 = figure(1)
plot(LVec,averageExtinctionTimeVec,'*')
xlabel('Shedding length in each shedding event','FontSize',30);
ylabel('Average extinction time','FontSize',30);
% set(gca,'XLim',[0,20]);
set(gca,'YLim',[0.5,1]);
set(gca,'Box','Off')
set(gca,'FontSize',30);
set(gca,'TickDir','out');
print(fig1, '-depsc','averageExtinctionTime.eps')
save averageExtinctionTimeVec.dat averageExtinctionTimeVec -ascii

fig2 = figure(2)
plot(LVec,probExtinctionVec,'*')
xlabel('Shedding length in each shedding event','FontSize',30);
ylabel('Probability of Extinction','FontSize',30);
% set(gca,'XLim',[0,20]);
set(gca,'YLim',[0.5,1]);
set(gca,'Box','Off')
set(gca,'FontSize',30);
set(gca,'TickDir','out');
print(fig2, '-depsc','probabilityOfExtinction.eps')
save LVec.dat LVec -ascii
save probExtinctionVec.dat probExtinctionVec -ascii

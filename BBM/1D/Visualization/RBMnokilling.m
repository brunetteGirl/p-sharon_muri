% Binary fusion + exponentially distributed life time + reflected Brownian
% motion
clear all; close all; clc;


% model parameters
    doublingTime = 1/3; % unit: hours
    a = log(2)/doublingTime; % unit: per hour; Malthusian growth rate
    lambda = 3; % unit: per hour; parameter of exponential distribution; inverse of average life time

    if lambda < a
        error('lambda has to be larger than a');
    end

    van_pr = 0.5*(1-log(2)/lambda/doublingTime); % vanishing probability of binary fusion (0 with van_pr, 2 with 1 - van_pr).
    Galton_Watson_extinction_p = (1 - sqrt(1-4*(1-van_pr)*van_pr))/2/(1-van_pr); % theoretical extinction pr without shedding
    % The larger lambda, the larger van_pr, the larger
    % Galton_Watson_extinction_p is.

% computational parameters
    dt = 0.01; % 36 seconds
    maxt = 4; % 4 hours
    tspan = 0:dt:maxt;
    nsteps = round(maxt/dt)+1; % num of time steps
    c_step = ceil(nsteps/100);
    
% initial conditions
    ini_part = 1; % begin with 1 particle
    pt_coor = [0 0]; % initial condition is (t0, x0) = (0, 0); 
    % pt_coor records (t,x) data, eventually becomes n by 2 matrix

% initializing remaining lifetime - exp(lambda) distributed
    pt_life = -1/lambda*log(rand(ini_part, 1)); % Is this correct???
    % Eventually a column vector

% create the first frame for the movie
    pt_conf = cell(1, 1); 
    pt_conf{1} = pt_coor; 

% setting up flags
    extinct = 0; % flag for the whole process, 0 if not extinct by maxt, 1 if extinct by then

for t=2:nsteps
   
   % shorten the remaining lifetime by dt, pt_life is a vector
       pt_life = pt_life - dt;
   
   % generate new coordinates for the particles alive:
   % add the increment of BM
       i_alive = find(pt_life > 0)
       pt_coor(i_alive,1) = tspan(t);
       pt_coor(i_alive,2) = reflectedBrownianMotion(pt_coor(i_alive,2),dt,0,1);
   
   % find dying particles and decide if they will die 
   % or will divide
       i_dying = find(pt_life<=0)
       runi = rand(1, length(i_dying)); % the dice roll
       % that determines reproduction or death
       
       % set the vanished particles to NaN
       pt_life(i_dying(find(runi<=van_pr))) = NaN; 
       % death with probability van_pr
   
       % replicate the particles that need that
       i_rep = i_dying(find(runi>van_pr)); 
       nrep = length(i_rep); % num of particles that get to replicate
       % which matters when you create new life times
   
       % generate new lifetimes for the parents
       pt_life(i_rep) = -1/lambda*log(rand(nrep, 1));
   
       % generate lifetimes for the children and add to the array
       pt_life = [pt_life; -1/lambda*log(rand(nrep, 1))]
   
   % add the coordinates of one of the newborn particles to the
   % array 
       pt_coor(i_rep,1) = pt_coor(i_rep,1) + dt;
       pt_coor = [pt_coor; pt_coor(i_rep, :)]; % This sentence doesn't
       % delete the dead particles.
       % pt_coor = [pt_coor(i_alive, :); pt_coor(i_rep, :)]

   % create a new frame
       pt_conf = [pt_conf cell(1, 1)];
       
       % add the particles still alive and the replicating particles
       pt_conf{t} = [pt_coor(i_alive, :); pt_coor(i_rep, :)];
       pt_conf{t}
   % test if there are any particles alive
       if all(isnan(pt_life))
         extinct = 1;
         break;
       end

   % display the progress
       if (rem(t, c_step)==0)
          fprintf('\r %i%% done', t/c_step);
       end
end

% printing process bar
    fprintf(1, '\r 100%% done\n');

    if (extinct)
        fprintf('\nExtinct after t=%f\n',(t-1)*dt);  
    else
        fprintf('\n%d particles alive after t=%f\n', ...
           size(pt_conf{nsteps}, 1), maxt);  
    end

% plotting
    bbmplot(pt_conf,1)
    
% saving data
    save pt_conf
function computeQOf0t(NSimulations,smallestSheddingLength)
addpath /home/zhou.494/Documents/MATLAB/p-Sharon_MURI/BBM/1D/sheddingRate/CompareWithPDE/OneParticleDifferentLocations

% NSimulations = 5000;
% smallestSheddingLength = 0.00125;
sheddingRateVec = [1 2 3 4 5 6 8 10 12 15 20 30 40 60 120]; %shedding once for every 1 BBM steps, 2 steps, ... 20 steps.
LVec = smallestSheddingLength.*sheddingRateVec;
averageExtinctionTimeVec = zeros(size(LVec));
probExtinctionVec = zeros(size(LVec));
for ind = 1:length(LVec)
    L = LVec(ind);
    sheddingRate = sheddingRateVec(ind);
    [averageExtinctionTime,probExtinction] = simulationsFunc(NSimulations,L,sheddingRate);
    averageExtinctionTimeVec(ind) = averageExtinctionTime;
    probExtinctionVec(ind) = probExtinction;
end

save(['averageExtinctionTimeVecL' num2str(smallestSheddingLength) '.dat'],'averageExtinctionTimeVec','-ascii')

fig1 = figure(1)
plot(LVec,probExtinctionVec,'*')
xlabel('Shedding length in each shedding event','FontSize',30);
ylabel('Probability of Extinction','FontSize',30);
set(gca,'XLim',[0,LVec(end)]);
set(gca,'YLim',[0.5,1]);
set(gca,'Box','Off')
set(gca,'FontSize',30);
set(gca,'TickDir','out');
title(['Shedding length =' num2str(smallestSheddingLength)]);
%print(fig1, '-depsc','probExtinctionVecWRTstartingLocations.eps')
print(fig1,'-depsc',['probExtinctionVec' num2str(smallestSheddingLength) '.eps'])
save(['LVec' num2str(smallestSheddingLength) '.dat'],'LVec','-ascii')
save(['probExtinctionVecWRTstartingLocationsL' num2str(smallestSheddingLength) '.dat'],'probExtinctionVec','-ascii')

fig2 = figure(2)
survivalProbabilityVec = 1-probExtinctionVec;
YAxis = -log(survivalProbabilityVec);
plot(LVec,YAxis,'*')
xlabel('Shedding length in each shedding event','FontSize',30);
ylabel('-log(Survival Probability)','FontSize',30);
set(gca,'XLim',[0,LVec(end)]);
%set(gca,'YLim',[0.5,1]);
set(gca,'Box','Off')
set(gca,'FontSize',30);
set(gca,'TickDir','out');
title(['Shedding length =' num2str(smallestSheddingLength)]);
print(fig2,'-depsc',['survivalProbabilityLogScale' num2str(smallestSheddingLength) '.eps'])
end
function [extinct, extinctionTime]=OneParticleAnimation(L,sheddingRate)
% Input: sheddingRate is how many BBM steps it goes through before one
% shedding event. L is the length shed.
% 
% MULTIBBM Simulates a branching Brownian motion in the plane and make
%   an animation. Assuming sigma = 1 for the BM.
%
%   Initially there is 1 particle at location 0. The
%   particle follows a reflected Brownian motion in 1D and has
%   exp(lambda) distributed lifetime. When the lifetime ends, the 
%   particle either divides into two particles with probability 1-van_pr
%   or dies with probability van_pr.
%
% [pt_conf]=multibbm(nu, lambda, van_pr, maxt
%                    [, dt, domain]) 
% 
% Inputs: 
%   nu - intensity of the Poisson process for the initial
%     configuration 
%   lambda - parameter of the exponential distribution
%     of the lifetime (note that expectation=1/lambda)
%   van_pr - probability for a particle to vanish
%   maxt - time interval
%   dt - time discretisation step. Optional, default dt=0.01.
%   domain - bounds for the region. A 4-dimensional vector in
%     the form [x_min x_max y_min y_max]. Optional, default value
%     [0 10 0 10]. ??? Why are there bounds for the region? If there are
%     bounds there must be boundary conditions right? Or is it the
%     simulation domain which is just assumed to be very big?
%
% Outputs:
%   pt_conf - "configuration of the particles". A cell array
%     describing the system dynamics. An element k is a N_k x 2
%     matrix with the coordinates of the particles alive after time
%     k*dt. 
%

 %if (nargin<1) % default parameter values
   nu = 0.7; % intensity of the Poisson process
   lambda = 1; % parameter of the lifetime distribution
   van_pr = 0.5;  % probability to vanish
   dt = 0.01; % small simulation step
   maxt = sheddingRate*dt;   % time interval for one shedding event to happen
   %totalSimuTime = 140*maxt; % 140 shedding events
   totalSimuTime = 24;
   totalSkinDepth = 10;
   tspan = 0:dt:totalSimuTime;
%end

 % if (nargin<5) % default parameter values 
   
   domain = [0 totalSkinDepth 0 totalSimuTime]; 
 % end

 disp('Generating BBM');
 
 xmin = domain(1); % bounds of the spatial domain
 xmax = domain(2); % bounds of the spatial domain
 ymin = domain(3); % bounds of the time domain
 ymax = domain(4); % bounds of the time domain
 clear domain;

% Initial Condition: one particle at 0
 ini_part = 1;
 particleCoordinates = [0 0]; % initial condition is (x0, t0) = (0, 0)
%  particleCoordinates = 0; % initial location is 0
% remaining lifetime for each initial particle - exp(lambda) distributed
 pt_life = -1/lambda*log(rand(ini_part, 1)); 
 
% create the first frame for the movie
 pt_conf = cell(1, 1); 
 pt_conf{1} = particleCoordinates; 

 extinct = 0; % flag for the whole process, 1 means extinction
 extinctionTime = 0; % if there's no extinction, then it's just 0.
 nsteps = round(maxt/dt)+1;  

 c_step = ceil(nsteps/100);
 
 numShedding = round(totalSimuTime/maxt);
 for T = 1:numShedding
     
     for t=1:nsteps

       % shorten the remaining lifetime by dt, so if pt_life remain > 0 that
       % means that particle will do BM; otherwise it will die or divide
       pt_life = pt_life-dt;

       % generate new coordinates for the particles alive:
       %     add the increment of BM (or RBM)
       i_alive = find(pt_life>=0);
       %particleCoordinates(i_alive) = particleCoordinates(i_alive) ...
                             %+randn(length(i_alive), 1)*dt^0.5; 
                             % ??? Is this a correct simulation of Brownian motion? what if sigma is
       % not 1?
       particleCoordinates(i_alive,1) = reflectedBrownianMotion(particleCoordinates(i_alive),dt,0,1);
       particleCoordinates(i_alive,2) = tspan(t);

       % find dying particles and decide if they will die 
       % or will divide
       i_dying = find(pt_life<0);
       runi = rand(1, length(i_dying));
       % set the vanished particles to NaN
       pt_life(i_dying(find(runi<=van_pr))) = NaN; 

       % replicate the particles that need that
       i_rep = i_dying(find(runi>van_pr)); 
       nrep = length(i_rep);
       % generate new lifetimes for the parents
       pt_life(i_rep) = -1/lambda*log(rand(nrep, 1));
       % generate lifetimes for the children and add to the array
       pt_life = [pt_life; -1/lambda*log(rand(nrep, 1))];

       % add the coordinates of one of the newborn particles to the
       % array 
       particleCoordinates = [particleCoordinates; particleCoordinates(i_rep,:)];   

       % test if there are any particles alive
       if all(isnan(pt_life))
         extinct = 1;
         break;
       end

       % create a new frame
       pt_conf = [pt_conf cell(1, 1)];
       % add the particles alive and the "second child" particles
       pt_conf{(T-1)*nsteps+t+1} = [particleCoordinates(i_alive); particleCoordinates(i_rep)];

       % display the progress
       if (rem(t, c_step)==0)
          fprintf('\r %i%% done', t/c_step);
       end
     end

     fprintf(1, '\r 100%% done\n');

     if (extinct)
       fprintf('\nExtinct after t=%f\n',(t-1)*dt);
       extinctionTime = (t-1)*dt;
     else
       fprintf('\n%d particles alive after t=%f\n', ...
               size(pt_conf{nsteps}, 1), maxt);   
     end

    % test if there are any particles alive
    if extinct == 1;
        break;
    end
    %      find the particles to the left of the shedding point and eliminate
    %     them
        particleCoordinates = particleCoordinates - L;
        pt_life(find(particleCoordinates<0)) = NaN;
    end
 % animate
 figure(1)
 fmat = bbmplot2D(pt_conf);
 
end

% This script is to make a movie of 1 realization of the stochastic process
clear all; close all; clc;

% OneParticleAnimation(0.1,1);
nu = 0.7; % intensity of the Poisson process
lambda = 20.0; % parameter of the lifetime distribution
van_pr = 0.5;  % probability to vanish

maxt = 0.7;  
dt = 0.01;
skinDepth = 10;
%tspan = 0:dt:maxt;
%domain = [0 skinDepth 0 maxt];
pt_conf = multibbm1D(lambda, van_pr, maxt, dt, skinDepth); 
bbmplotSpaceTime(pt_conf,1);

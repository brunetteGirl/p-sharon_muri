function [averageExtinctionTime,probExtinction] = simulationsFunc(NSimulations,L,sheddingRate)

extinctionTimes =zeros(1,NSimulations);
extinctionVec = zeros(1,NSimulations);
for run = 1:NSimulations
    [extinction,extinctionTime] = multibbm2OneParticle(L,sheddingRate);
    extinctionTimes(run) = extinctionTime;
    extinctionVec(run) = extinction;
end

averageExtinctionTime = sum(extinctionTimes)/sum(extinctionVec);
probExtinction = sum(extinctionVec)/length(extinctionVec);
end
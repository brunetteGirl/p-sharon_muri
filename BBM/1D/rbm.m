% This is a MATLAB code provided in the book Handbook for Monte Carlo
% Methods and cite in Wikipedia. The book talks about the algorithm for
% simulating the process. This code is based on algorithm 5.26
% In this code, X is the Reflected Brownian Motion process. B is the
% non-reflected Brownian Motion process with drift mu and diffusion coefficient sigma^2.
% Updates: May 28, rescaledX is the rescaled process with sigma = 1. 

clear all; close all; clc;

n=10^4; % number of time steps
h=10^(-3); % h is the time step for approximating this continuous-time process
t=h.*(0:n); % t is the time span vector
sigma = 1; % sigma^2 is the diffusion parameter.
mu = 0; % mu is the drift for the Brownian Motion
rescaledmu = mu/sigma; % rescale the drift for the rescaled process
X=zeros(1,n+1); % Initializing a vector for the location of the particle at each time step
M=X; B=X; % Initializing every vector as 0 vectors.
rescaledX = X; rescaledB = X;
B(1)=3; % The particle starts at position B(1) for the non-reflected Brownian Motion.
rescaledB(1) = B(1)/sigma;
X(1)=3; % The particle starts at position X(1) for the reflected Brownian Motion.
rescaledX(1) = X(1)/sigma;
for k=2:n+1
  Y=sqrt(h)*randn; % see step 2 of Algotirhm 5.26
  U=rand(1); % see step 3 of Algotirhm 5.26
  %rescaledB(k)=rescaledB(k-1)+ rescaledmu*h-Y;
  M=(Y + sqrt(Y^2-2*h*log(U)))/2; % see step 3 of Algotirhm 5.26
  rescaledX(k)=max(M-Y,rescaledX(k-1)+h*rescaledmu-Y); % see step 4 of Algotirhm 5.26
end
X = sigma*rescaledX;
%B = sigma*rescaledB;
% subplot(2,1,1)
plot(t,X,'k-');
% subplot(2,1,2)
% plot(t,X-B,'k-');

clear all; close all; clc;

tic
NSimulations = 5000;
LVec = linspace(0.0025,0.0975,20);
averageExtinctionTimeVec = zeros(size(LVec));
probExtinctionVec = zeros(size(LVec));
for ind = 1:length(LVec)
    L = LVec(ind);
    [averageExtinctionTime,probExtinction] = simulationsFunc(NSimulations,L);
    averageExtinctionTimeVec(ind) = averageExtinctionTime;
    probExtinctionVec(ind) = probExtinction;
end
toc

fig1 = figure(1)
plot(LVec,averageExtinctionTimeVec,'-')
print(fig1, '-depsc','averageExtinctionTime.eps')

fig2 = figure(2)
plot(LVec,probExtinctionVec,'-')
print(fig2, '-depsc','probabilityOfExtinction.eps')
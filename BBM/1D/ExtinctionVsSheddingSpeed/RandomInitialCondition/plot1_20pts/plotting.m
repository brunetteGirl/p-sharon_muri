clear all; close all; clc;

% Lvec = [0 0.01];
% extinctionOrNot = [0.0160 0.03];
% averageExtinctionTimes = [0.42 0.3939];

NSimulations = 5000;
load Lvec.dat
load probExtinctionVec.dat

fig2 = figure(2)
plot(Lvec,probExtinctionVec,'-')
xlabel('L','FontSize',30);
ylabel('Probability of Extinction','FontSize',30);
% set(gca,'XLim',[0,20]);
% set(gca,'YLim',[0,20]);
set(gca,'Box','Off')
set(gca,'FontSize',30);
set(gca,'TickDir','out');
print(fig2, '-depsc','probabilityOfExtinction.eps')
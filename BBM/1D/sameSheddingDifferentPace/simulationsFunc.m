function [averageExtinctionTime,probExtinction] = simulationsFunc(NSimulations,L,sheddingRate)

% NSimulations = 5000;
extinctionTimes =zeros(1,NSimulations);
extinctionVec = zeros(1,NSimulations);
for run = 1:NSimulations
    [extinction,extinctionTime] = multibbm2(L,sheddingRate);
    extinctionTimes(run) = extinctionTime;
    extinctionVec(run) = extinction;
end

averageExtinctionTime = sum(extinctionTimes)/sum(extinctionVec);
probExtinction = sum(extinctionVec)/length(extinctionVec);

%save averageExtinctionTime.dat averageExtinctionTime -ascii
%save probExtinction.dat probExtinction -ascii
end
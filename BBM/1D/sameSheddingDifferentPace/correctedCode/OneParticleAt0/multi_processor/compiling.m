clear all; close all; clc;

files = dir('*.dat');
for i=1:length(files)
    eval(['load ' files(i).name ' -ascii']);
    filename = files(i).name;
    averageWaitingTimeVec(i) = str2num(filename(25:end-4));
    variablename = filename;
    positionOfdots = find(filename=='.');
    variablename(positionOfdots(1)) = '_';
    variablename(positionOfdots(2):end) = [];
    eval(['probabilityOfExtinctioinVec(i) = ' variablename '(1);']);
end

% save probabilityOfExtinctioinVec10.dat probabilityOfExtinctioinVec -ascii

fig1 = figure(1)
plot(averageWaitingTimeVec/0.05,probabilityOfExtinctioinVec,'o')
xlabel('Average waiting time of extreme events (years)','FontSize',30);
ylabel('Probability of extinction','FontSize',30);
% set(gca,'XLim',[0,20]);
% set(gca,'YLim',[0,20]);
set(gca,'Box','Off')
set(gca,'FontSize',30);
set(gca,'TickDir','out');
%legend(['b = 0.08, Numer of MC sims = 1000'])
%legend('boxoff')
print(fig1, '-depsc','ProbabilityOfExtinction.eps')

fig2 = figure(2)
plot(averageWaitingTimeVec/0.05,log(probabilityOfExtinctioinVec),'o')
xlabel('Average waiting time of extreme events (years)','FontSize',30);
ylabel('ln(Probability of extinction)','FontSize',30);
% set(gca,'XLim',[0,20]);
% set(gca,'YLim',[0,20]);
set(gca,'Box','Off')
set(gca,'FontSize',30);
set(gca,'TickDir','out');
%legend(['b = 0.08, Numer of MC sims = 1000'])
%legend('boxoff')
print(fig2, '-depsc','lnProbabilityOfExtinction.eps')
clear all; close all; clc;

tic
NSimulations = 5000;
% smallestSheddingLengthVec = linspace(0.0025,0.0975,19);
% smallestSheddingLengthVec = linspace(0.0001,0.001,10);
smallestSheddingLengthVec = linspace(0.00001,0.0001,10);
parfor ind = 1:length(smallestSheddingLengthVec)
    smallestSheddingLength = smallestSheddingLengthVec(ind);
    computeQOf0t(NSimulations,smallestSheddingLength);
end
toc

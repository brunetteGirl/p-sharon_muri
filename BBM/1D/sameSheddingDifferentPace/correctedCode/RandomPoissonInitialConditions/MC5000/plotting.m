clear all; close all; clc;

load probExtinctionVec.dat
load LVec.dat
fig1 = figure(1)
plot(LVec,probExtinctionVec,'*')
xlabel('Shedding length in each shedding event','FontSize',30);
ylabel('Probability of Extinction','FontSize',30);
% set(gca,'XLim',[0,20]);
set(gca,'YLim',[0.5,1]);
set(gca,'Box','Off')
set(gca,'FontSize',30);
set(gca,'TickDir','out');
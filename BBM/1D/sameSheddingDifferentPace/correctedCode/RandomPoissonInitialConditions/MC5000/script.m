clear all; close all; clc;

tic
NSimulations = 5000;
smallestSheddingLength = 0.0025;
sheddingRateVec = [1 2 3 4 5 6 8 10 12 15 20 30 40 60 120]; %shedding once for every 1 BBM steps, 2 steps, ... 20 steps.
LVec = smallestSheddingLength.*sheddingRateVec;
averageExtinctionTimeVec = zeros(size(LVec));
probExtinctionVec = zeros(size(LVec));
for ind = 1:length(LVec)
    L = LVec(ind);
    sheddingRate = sheddingRateVec(ind);
    [averageExtinctionTime,probExtinction] = simulationsFunc(NSimulations,L,sheddingRate);
    averageExtinctionTimeVec(ind) = averageExtinctionTime;
    probExtinctionVec(ind) = probExtinction;
end
toc

fig1 = figure(1)
plot(LVec,averageExtinctionTimeVec,'-')
print(fig1, '-depsc','averageExtinctionTime.eps')

fig2 = figure(2)
plot(LVec,probExtinctionVec,'-')
print(fig2, '-depsc','probabilityOfExtinction.eps')
save LVec.dat LVec -ascii
save probExtinctionVec.dat probExtinctionVec -ascii

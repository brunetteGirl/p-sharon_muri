% This is a script that runs multibbm.m and simulates the BBM with
% reflected boundary and shedding, and runs the simulation multiple times.

clear all; close all; clc;
tic

NSimulations = 5000;
for run = 1:NSimulations
    [extinction,extinctionTime] = multibbm1;
    extinctionTimes(run) = extinctionTime;
    extinctionVec(run) = extinction;
end

averageExtinctionTime = sum(extinctionTimes)/sum(extinctionVec);
probExtinction = sum(extinctionVec)/length(extinctionVec);

toc

save averageExtinctionTime.dat averageExtinctionTime -ascii
save probExtinction.dat probExtinction -ascii
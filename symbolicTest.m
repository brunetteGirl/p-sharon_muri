clear all; close all; clc;

syms r t L k
M = exp(r*t-L^2/(4*k*t))/sqrt(pi*k*t);


y = 1-erf(1/sqrt(t));
ezplot(y)
